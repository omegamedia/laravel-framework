<?php namespace Illuminate\Encryption;

use Exception;
use Illuminate\Contracts\Encryption\DecryptException;
use Symfony\Component\Security\Core\Util\StringUtils;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Illuminate\Contracts\Encryption\Encrypter as EncrypterContract;

class Encrypter implements EncrypterContract {

	/**
	 * Create a new encrypter instance.
	 *
	 * @param  string  $key
	 */
	public function __construct($key) {}

	/**
	 * Encrypt the given value.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function encrypt($value)
	{
		return $value;
	}

	/**
	 * Decrypt the given value.
	 *
	 * @param  string  $payload
	 * @return string
	 */
	public function decrypt($payload)
	{
		return $payload;
	}

	/**
	 * Set the encryption key.
	 *
	 * @param  string  $key
	 * @return void
	 */
	public function setKey($key) {}

	/**
	 * Set the encryption cipher.
	 *
	 * @param  string  $cipher
	 * @return void
	 */
	public function setCipher($cipher) {}

	/**
	 * Set the encryption mode.
	 *
	 * @param  string  $mode
	 * @return void
	 */
	public function setMode($mode) {}

}
